
const stringCamelCaseToSnakeCase = str=>str.replace(/([A-Z])/g, '_$1').toLowerCase();
const stringSnakeCaseToCamelCase = str=>str.replace(/_(.)/g, (_, a)=>a.toUpperCase());

const stringPascalCaseToSnakeCase = str=>stringCamelCaseToSnakeCase(str.replace(/^\w/, c => c.toLowerCase()));
const stringSnakeCaseToPascalCase = str=>stringSnakeCaseToCamelCase(str).replace(/^\w/, c => c.toUpperCase());


const camelCaseToSnakeCase = convertCase(stringCamelCaseToSnakeCase)
const snakeCaseToCamelCase = convertCase(stringSnakeCaseToCamelCase)

const pascalCaseToSnakeCase = convertCase(stringPascalCaseToSnakeCase)
const snakeCaseToPascalCase = convertCase(stringSnakeCaseToPascalCase)

const noConvert = a=>a;


function convertCase(converter){
    function c(item){
        let res;
        if (isObject(item)){
            res = {}
            Object.keys(item).forEach(key=>{
                res[converter(key)] = c(item[key])
            })
        }else if (isArray(item)){
            res = item.map(c)
        }else{
            res = item
        }
        return res
    }
    return c;
}



function isObject(o){
    return Object.prototype.toString.call(o) === '[object Object]'
}

function isArray(o){
    return Object.prototype.toString.call(o) === '[object Array]'
}

const DEFAULT_CONVERTER = Symbol('default_converter')

const caseConverters = {
    "camelCase": {
        toSnakeCase: camelCaseToSnakeCase,
        fromSnakeCase: snakeCaseToCamelCase
    },
    "snake_case": {
        toSnakeCase: noConvert,
        fromSnakeCase: noConvert
    },
    "PascalCase": {
        toSnakeCase: pascalCaseToSnakeCase,
        fromSnakeCase: snakeCaseToPascalCase
    }
}

caseConverters[DEFAULT_CONVERTER] = caseConverters['camelCase']

module.exports = {
    stringCamelCaseToSnakeCase,
    stringSnakeCaseToCamelCase,
    stringPascalCaseToSnakeCase,
    stringSnakeCaseToPascalCase,
    camelCaseToSnakeCase,
    snakeCaseToCamelCase,
    pascalCaseToSnakeCase,
    snakeCaseToPascalCase,
    DEFAULT_CONVERTER,
    caseConverters
}
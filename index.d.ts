export const stringCamelCaseToSnakeCase: Function
export const stringSnakeCaseToCamelCase: Function

export const stringPascalCaseToSnakeCase: Function
export const stringSnakeCaseToPascalCase: Function


export const camelCaseToSnakeCase: Function
export const snakeCaseToCamelCase: Function

export const pascalCaseToSnakeCase: Function
export const snakeCaseToPascalCase: Function

export const DEFAULT_CONVERTER: Symbol

export const caseConverters: Object